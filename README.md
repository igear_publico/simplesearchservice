### Dependencias

Para el funcionamiento del servicio SimpleSearchService es necesario disponer de una tabla/vista en Postgres denominada v_busquedas que contendra todos los elementos geográficos sobre los que se puede buscar. Dicha vista/tabla debe tener los siguientes campos:
    + objectid: identificador único de la feature en su capa. Tipo texto.  
    + codigo: código de la feature. Tipo texto.  
    + nombre: nombre de la feature. Tipo texto.  
    + fonetica: fonetica del nombre de la feature. Tipo texto  
    + c_muni_ine: código INE del municipio en el que se encuentra la feature. Tipo entero.  
    + municipio: nombre del municipio en el que se encuentra la feature. Tipo texto.  
    + fonetica_municipio: fonética nombre del municipio en el que se encuentra la feature. Tipo texto.  
    + fonetica_localidad: para calles, fonética de la localidad en que se encuentra. Tipo texto.  


### Instalación

Para instalarlo, generar el war y desplegar en el servidor de aplicaciones