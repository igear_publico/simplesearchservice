package es.ideariumConsultores.search.geonames;

import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import es.ideariumConsultores.search.simple.BusquedaSimple;
import es.ideariumConsultores.search.simple.Resultado;

public class Servlet extends HttpServlet {



	/**
	 * petici�n por GET
	 * @param request petici�n que deber� contener el par�metro texto con la cadena a buscar y opcionalmente
	 * los par�mtros inicio y total para indicar la el primer resultado y el total a devolver y el par�metro bbox
	 * para restringir la b�squeda a dicha regi�n geogr�fica.
	 * @param response resultados de la b�squeda
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		BusquedaSimple.conexError.clear();
		String texto=request.getParameter("texto").trim();//Quitamos espacios en blanco

		String indice=request.getParameter("inicio");
		if ((indice == null) || (indice.length() ==0)){
			indice ="1";
		}
		
		String maxResults=request.getParameter("maxResults"); // m�ximo num de resultados a buscar
		if ((maxResults != null) && (maxResults.length() ==0)){
			maxResults =null;
		}
		String total=request.getParameter("total");  //n� de resultados a devolver
		if ((total == null) || (total.length() ==0)){
			total ="20";
		}
		if ((maxResults !=null)&&(Integer.parseInt(maxResults)<Integer.parseInt(total))){
			total = maxResults;
		}
		String bbox=request.getParameter("bbox");
		if ((bbox != null) && (bbox.length() ==0)){
			bbox =null;
		}
		String app = request.getParameter("app");
		if ((app != null) && (app.length() ==0)){
			app = "";
		}
		BusquedaSimple.logger.info("texto "+texto + "(app: " + app + ")");

		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage outgoingMessage = messageFactory.createMessage();

			SOAPPart soappart = outgoingMessage.getSOAPPart();
			SOAPEnvelope envelope = soappart.getEnvelope();
			SOAPBody body = envelope.getBody();
			SOAPBodyElement responseElement = body.addBodyElement(envelope.createName("SearchResponse"));
			
			// realizar la b�squeda
			int inicio = Integer.parseInt(indice);
			int max =Integer.MAX_VALUE; 
			
			if(maxResults != null){
					max=Integer.parseInt(maxResults);
			}
			int totalInt = Integer.parseInt(total);
			String texto_fon = BusquedaSimple.getFonetica(texto);
			Resultado toponimos= BusquedaSimple.doDBSearch(texto_fon,"toponimo",BusquedaSimple.ATTR_FONETICA,inicio,totalInt,max, BusquedaSimple.properties.getProperty("TOPO_DB","conexSSS_carto"),false);
			ArrayList<Resultado> resultados = new ArrayList<Resultado>();
			resultados.add(toponimos);
			int count = 0;
			
				count += toponimos.count;
			
			if (count < max){
				Resultado toponimos_like= BusquedaSimple.doDBSearch(texto_fon,"toponimo",BusquedaSimple.ATTR_FONETICA,Math.max(1,inicio-count),Math.min(totalInt,totalInt-count+inicio-1),max-count, BusquedaSimple.properties.getProperty("TOPO_DB","conexSSS_carto"),true);
				resultados.add(toponimos_like);
			}
			
			
			
			// componer la respuesta con los resultados de la b�squeda
			for (Resultado resultado: resultados){
				SOAPElement searchResult = responseElement.addChildElement(envelope.createName("SearchResult"));
				searchResult.addChildElement(envelope.createName("Type")).addTextNode(resultado.type);
				searchResult.addChildElement(envelope.createName("Count")).addTextNode(new Integer(resultado.count).toString());
				if(resultado.list != null)
					searchResult.addChildElement(envelope.createName("List")).addTextNode(resultado.list);
			}

			// enviar la respuesta
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("text/xml; charset=\"utf-8\"");
			OutputStream os = response.getOutputStream();
			outgoingMessage.writeTo(os);
			os.flush();
		} 
		catch(Throwable e) {
			BusquedaSimple.logger.error("Error en la b�squeda de "+texto, e);
			try{
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
			catch(Exception ex){
				
			}
		}
	}
}
