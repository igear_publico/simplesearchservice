package es.ideariumConsultores.search.simple;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import conexion.ConexionBD;



/**
 * Clase que implementa la b�squeda de un texto en diferentes buscadores y con diferentes par�metros en funci�n de la naturaleza del mismo (buscador de caja unica)
 * El m�todo principal de la clase es buscar(...)
 * @author Raquel
 *
 */
public  class BusquedaSimple {

	public static Properties properties;  // archivo de configuraci�n del servicio
	public  static Logger logger = Logger.getLogger("SimpleSearchService");
	//protected static ConexionBD conexionBD;
	static String vistaBD="v_busquedas";
	public static String ATTR_NOMBRE="nombre";
	public static String ATTR_CODIGO="codigo";
	public static String ATTR_FONETICA="fonetica";
	public static String ATTR_MUNI_INE="c_muni_ine";
	public static String ATTR_MUNICIPIO="fonetica_municipio";
	public static String ATTR_LOCALIDAD="fonetica_localidad";
	
	public static HashMap<String, Boolean> conexError = new HashMap<String, Boolean> ();
	/**
	 * carga el fichero de propiedades
	 * @param propertiesPath ruta completa del fichero de propiedades
	 */
	static public void initProperties(String propertiesPath){

		properties = new Properties();
		try{
			properties.load(new FileInputStream(propertiesPath));
		}
		catch(Exception ex){
			logger.error("Error al cargar el fichero de propiedades "+propertiesPath, ex);
			//	ex.printStackTrace();	
		}
		/* try {
				conexionBD = new ConexionBD();

				 } catch (Exception e) {
						logger.error("Error en la instanciación de las clases de conexión a base de datos", e);
					} */
	}

	/**
	 * Indica si el texto representa un n�mero entero
	 * @param texto
	 * @return
	 */
	static public boolean esEntero(String texto){
		return texto.matches("[0-9]*");
		/*try{
			Integer.parseInt(texto);
		}
		catch(NumberFormatException ex){
			return false;
		}
		return true;*/
	}

	/**
	 * Indica si el texto representa un n�mero real
	 * @param texto
	 * @return
	 */
	static protected boolean esReal(String texto){
		try{
			Double.parseDouble(texto);
		}
		catch(NumberFormatException ex){
			return false;
		}
		return true;
	}

	/**
	 * indica si el texto comienza por un n�mero
	 * @param texto
	 * @return
	 */
	static protected boolean empiezaPorNumero(String texto){
		return esEntero(texto.substring(0,1))||
				(texto.substring(0,1).equalsIgnoreCase("-")&& esEntero(texto.substring(1,2)));
	}

	/**
	 * indica si el texto es un c�digo que empieza por el prefijo indicado seguido de n�meros
	 * @param texto texto a comprobar si es c�digo
	 * @param prefijo prefijo por el que debe comenzar el texto para considerarlo c�digo
	 * @return true si texto empieza por el prefijo y el resto son n�meros, false en caso contrario
	 */
	static protected boolean esCodigo(String texto, String prefijo){
		if(texto.toUpperCase().startsWith(prefijo.toUpperCase())){
			return esEntero(texto.toUpperCase().replaceFirst(prefijo.toUpperCase(), ""));
		}
		return false;
	}

	/**
	 * Devuelve el n�mero de apariciones de una cadena de texto dentro de otra
	 * @param texto cadena de texto en la que contar
	 * @param textoAContar cadena de texto cuyas apariciones hay que contar
	 * @return n�mero de apariciones de textoAContar en texto
	 */
	static protected int numeroApariciones(String texto, String textoAContar){
		int count=0;
		int pos =texto.indexOf(textoAContar);
		while (pos>=0){
			count++;
			pos =texto.indexOf(textoAContar,pos+1);
		}
		return count;
	}

	/**
	 * Sustituye  el espacio en blanco por %20
	 * @param txt Cadena de texto a normalizar
	 * @return cadena sin espacios en blanco (sustituidos por %20)
	 */
	static protected String normalizaCadena(String txt, boolean replaceBlank){
		String txt_norm =txt;
		try{

			txt_norm =URLDecoder.decode(txt,"UTF-8");
		}
		catch(UnsupportedEncodingException ex){
			ex.printStackTrace();
		}
		txt_norm = txt_norm.replace("Ã¡", "a").replace("Ã©", "e").replace("Ã­", "i").replace("Ã³", "o").replace("Ãº", "u");
		txt_norm = txt_norm.replace("Ã�", "A").replace("Ã‰", "E").replace("Ã�", "I").replace("Ã“", "O").replace("Ãš", "U");
		txt_norm = txt_norm.replace("Ãœ", "U").replace("Ã¼", "u").replace("Ã±", "ny").replace("Ã‘", "NY").replace("Âº", "\u00BA");
		txt_norm = txt_norm.replace("&#201;", "E").replace("&#193;", "A").replace("&#205;", "I").replace("&#211;", "O").replace("&#218;", "U").replace("&#209;", "NY").replace("&#220;", "U");

		if (replaceBlank){
			txt_norm = txt_norm.replace(" ", "%20");
		}
		return txt_norm;

	}


	static protected String normalizaCadena(String txt){
		return normalizaCadena(txt,true);
	}

	/**
	 * Devuelve la cadena que representa la fon�tica del texto pasado como par�metro
	 * @param txt cadena de la que se obtendr� la fon�tica. No debe contener acentos ni Ã± (sustituir por n)
	 * @return cadena de texto que representa la fonetica del texto pasado como par�metro
	 */
	public static String getFonetica(String txt){
		logger.info("getFonetica "+txt);
		try{
			URL url = new URL(properties.getProperty("BD_GIS_FONETICA_URL")+normalizaCadena(txt));
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");

			//read the result from the server
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			//StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				if ((!line.startsWith("<"))&&(line.length()>0)){
					//	sb.append(line + '\n');
					connection.disconnect();
					logger.info("Fonetica "+line);
					return line;
				}
			}
			/*	connection.disconnect();
			return sb.toString();*/
		}
		catch(MalformedURLException ex){
			logger.error("Error al obtener la fonÃ©tica del texto "+txt, ex);
		}
		catch(Exception ex){
			logger.error("Error al obtener la fonÃ©tica del texto "+txt, ex);
		}
		logger.info("Fonetica original"+txt);
		return txt;		// si hay error se devuelve el propio texto de entrada
	}

	/**
	 * Busca el texto indicado como un tipo de entidad en el servicio correspondiente seg�n la naturaleza del mismo. Este
	 * es el m�todo invocado desde el jsp
	 * @param txt texto a buscar
	 * @param beginRecord �ndice del primer resultado a devolver
	 * @param maxRecords m�ximo n�mero de resultados a devolver
	 * @param bounding box para ordenar los resultados de las peticiones al gazetteer (opcional)
	 * @param properties_file_path ruta completa del archivo de propiedades
	 * @return resultado de la b�squeda compuesto por los resultados correspondientes a los tipos de b�squeda realizados
	 * separados por ##. Para cada tipo de b�squeda se devuelve, separados por ; el nombre del feature type buscado, el n�mero de resultados y
	 * si hay resultado la respuesta proporcionada por el servicio de busqueda.
	 */
	static public String buscar(String txt, String beginRecord, String maxRecords, String maxResults,String bbox, String properties_file_path ){
		return buscar(txt, beginRecord, maxRecords,maxResults, bbox, properties_file_path, "");
	}

	/**
	 * Busca el texto indicado como un tipo de entidad en el servicio correspondiente seg�n la naturaleza del mismo. Este
	 * es el m�todo invocado desde el jsp
	 * @param txt texto a buscar
	 * @param beginRecord �ndice del primer resultado a devolver
	 * @param maxRecords m�ximo n�mero de resultados a devolver
	 * @param bounding box para ordenar los resultados de las peticiones al gazetteer (opcional)
	 * @param properties_file_path ruta completa del archivo de propiedades
	 * @param app nombre de la aplicaci�n que realiza la b�squeda 
	 * @return resultado de la b�squeda compuesto por los resultados correspondientes a los tipos de b�squeda realizados
	 * separados por ##. Para cada tipo de b�squeda se devuelve, separados por ; el nombre del feature type buscado, el n�mero de resultados y
	 * si hay resultado la respuesta proporcionada por el servicio de busqueda.
	 */
	static public String buscar(String txt, String beginRecord, String maxRecords, String maxResults, String bbox, String properties_file_path, String app){

		initProperties(properties_file_path);


		ArrayList<Resultado> result =  buscar(txt, beginRecord, maxRecords,maxResults, bbox, false, app);

		String  response="";
		for (Resultado the_result: result){
			response = response +the_result.getResult()+"##";
		}
		return response;
	}

	static public String formatRefCat(String texto){
		StringTokenizer token = new StringTokenizer(texto,"-");
		if (token.countTokens()==0){
			token = new StringTokenizer(texto,":");
		}
		if (token.countTokens()==6){
			String c1= token.nextToken();
			String c2= completeCode(token.nextToken(),3);
			String c3= completeCode(token.nextToken(),3);
			if (c3.equalsIgnoreCase("000")){
				c3=c2;
			}
			String c4= completeCode(token.nextToken(),2);
			String c5= completeCode(token.nextToken(),3);
			String c6= completeCode(token.nextToken(),5);
			return c1+c2+c3+c4+c5+c6;
		}
		return texto;
	}
	static public String completeCode(String code, int length){
		String result = code;
		while (result.length()<length){
			result ="0"+result;
		}
		return result;
	}

	static public void registraQuery(String txt) {
		registraQuery(txt, "");
	}

	static public void registraQuery(String txt, String app) {
		String charset = "UTF-8";

		try {
			String query = String.format("literal=%s&app=%s", URLEncoder.encode(txt, charset), app);
			URL url = new URL(properties.getProperty("BD_GIS_REGISTRABUSQUEDA_URL") + query);

			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();

			//leo la respuesta porque sino parece que no lo llega a registrar
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((rd.readLine()) != null)
			{
			}

			connection.disconnect();
		} catch (Exception e) {
			logger.error("Error al registrar la b�squeda "+ e);
		} finally {
		}

		/*
		connection.setDoOutput(true);
		connection.setRequestProperty("Accept-Charset", this.charset);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
		    connection.getOutputStream().write(query.getBytes(charset));
		 */
	}

	/**
	 * Busca el texto indicado como un tipo de entidad en el servicio correspondiente seg�n la naturaleza del mismo. Este
	 * es el m�todo invocado desde el jsp
	 * @param txt texto a buscar
	 * @param beginRecord �ndice del primer resultado a devolver
	 * @param maxRecords m�ximo n�mero de resultados a devolver
	 * @param bounding box para ordenar los resultados de las peticiones al gazetteer (opcional)
	 * @return resultado de la b�squeda compuesto por los resultados correspondientes a los tipos de b�squeda realizados
	 * separados por ##. Para cada tipo de b�squeda se devuelve, separados por ; el nombre del feature type buscado, el n�mero de resultados y
	 * si hay resultado la respuesta proporcionada por el servicio de busqueda.
	 */
	static public ArrayList<Resultado> buscar(String txt, String beginRecord, String maxRecords, String maxResults, String bbox){
		return buscar(txt, beginRecord, maxRecords,maxResults, bbox, false, "");
	}

	/**
	 * Busca el texto indicado como un tipo de entidad en el servicio correspondiente seg�n la naturaleza del mismo. Este
	 * es el m�todo invocado desde el jsp
	 * @param txt texto a buscar
	 * @param beginRecord �ndice del primer resultado a devolver
	 * @param maxRecords m�ximo n�mero de resultados a devolver
	 * @param bounding box para ordenar los resultados de las peticiones al gazetteer (opcional)
	 * @param dontCare booleano para poder distinguir la sobrecarga de m�todos 
	 * @param app nombre de la aplicaci�n que realiza la b�squeda 
	 * @return resultado de la b�squeda compuesto por los resultados correspondientes a los tipos de b�squeda realizados
	 * separados por ##. Para cada tipo de b�squeda se devuelve, separados por ; el nombre del feature type buscado, el n�mero de resultados y
	 * si hay resultado la respuesta proporcionada por el servicio de busqueda.
	 */
	static public ArrayList<Resultado> buscar(String txt, String beginRecord, String maxRecords, String maxResults, String bbox, boolean dontCare, String app){
		registraQuery(txt, app);

		ArrayList<Resultado> resultado = new ArrayList<Resultado>();
		// Analizar la cadena para definir el tipo de b�squeda y realizar la b�squeda
		String texto=txt.trim();
		int total = Integer.parseInt(maxRecords);
		int inicio = Integer.parseInt(beginRecord);
		int max =Integer.MAX_VALUE; 
		if(maxResults != null){
			max=Integer.parseInt(maxResults);
		}

		if (esEntero(texto)){
			if(texto.length()==3){ // hoja 50000
				resultado.add(doDBSearch(texto, properties.getProperty("HOJA50000_FTTYPE"),
						ATTR_CODIGO,inicio,total,max,properties.getProperty("HOJA50000_DB","conexSSS_carto"),false));
				return resultado;
			}
			else if(texto.length()==5){ // codigo INE municipio o codigo postal
				Resultado cp = doDBSearch(texto, properties.getProperty("CP_FTTYPE"),
						ATTR_CODIGO,inicio,total,max,properties.getProperty("CP_DB","conexSSS_carto"),false);
				resultado.add(cp);
				if (cp.count<max){
					resultado.add( doDBSearch(texto, properties.getProperty("MUNICIPIO_FTTYPE"),
							ATTR_CODIGO,Math.max(1,inicio-cp.count),Math.min(total,total-cp.count+inicio-1),max-cp.count,properties.getProperty("MUNICIPIO_DB","conexSSS_carto"),false));
				}
				return resultado;
			}
			else if(texto.length()==4){ // zona salud
				resultado.add( doDBSearch(texto, properties.getProperty("ZBS_FTTYPE"),
						ATTR_CODIGO,inicio,total,max,properties.getProperty("ZBS_DB","conexSSS_carto"),false));
				return resultado;

			}
			else if(texto.length()==8){ // matrícula montes
				resultado.add( doDBSearch(texto, properties.getProperty("MONTE_FTTYPE"),
						ATTR_CODIGO,inicio,total,max,properties.getProperty("MONTE_DB","conexSSS_carto"),false));
				return resultado;

			}
			else if((texto.length()==6)||(texto.length()==7)){ // matrícula COTO
				resultado.add( doDBSearch(texto, properties.getProperty("COTO_FTTYPE"),
						ATTR_CODIGO,inicio,total,max,properties.getProperty("COTO_DB","conexSSS_carto"),false));
				return resultado;

			}
			else if(texto.length()<=2){ // zonas escolares o sectores sanitarios
				Resultado zonaedu =  doDBSearch(texto, properties.getProperty("ZONAEDU_FTTYPE"),
						ATTR_CODIGO,inicio,total,max,properties.getProperty("ZONAEDU_DB","conexSSS_carto"),false);
				resultado.add(zonaedu);
				if (zonaedu.count<max){
					Resultado secsan = doDBSearch(texto, properties.getProperty("SECSAN_FTTYPE"),
							ATTR_CODIGO,Math.max(1,inicio-zonaedu.count),Math.min(total,total-zonaedu.count+inicio-1),max-zonaedu.count,properties.getProperty("SECSAN_DB","conexSSS_carto"),false);
					resultado.add(secsan);

				}
				return resultado;
			}

			else if(texto.matches("[0-9]*")&&(texto.length()==18)){ // parcela SIGPAC 
				String c3=texto.substring(5, 8);
				if (c3.equalsIgnoreCase("000")){
					String c2=texto.substring(2, 5);
					texto = texto.substring(0, 5)+c2+texto.substring(8, 18);
				}

				Resultado sigpac= doDBSearch(texto, properties.getProperty("SIGPAC_FTTYPE"),
						ATTR_CODIGO,inicio,total,max,properties.getProperty("SIGPAC_DB","conexSSS_carto"),false);
				resultado.add(sigpac);
				if (sigpac.count<max){

					Resultado reg_vit = doDBSearch(texto.toUpperCase(), properties.getProperty("REGVIT_FTTYPE"),
							ATTR_CODIGO,Math.max(1,inicio-sigpac.count),Math.min(total,total-sigpac.count+inicio-1),max-sigpac.count,properties.getProperty("REGVIT_DB","conexSSS_carto"),false);
					resultado.add(reg_vit);

				}
				return resultado;
			}
		}
		else if ((empiezaPorNumero(texto))&&((texto.indexOf(",")>0)||(texto.indexOf(";")>0)||(numeroApariciones(texto,":")==1)) ){  // empieza por n�mero y tiene alguna coma
			String separador = ",";
			if(texto.indexOf(";")>0){
				separador=";";
			}
			else if(texto.indexOf(":")>0){
				separador=":";
			}
			StringTokenizer token = new StringTokenizer(texto,separador);
			if(token.countTokens()==2){  // tiene una coma
				String parte1 = normalizaCadena(token.nextToken().trim(),false);
				String parte2 = normalizaCadena(token.nextToken().trim(),false);
				String huso="30";
				Double[] coords =null;
				String epsg="EPSG:25830";
				if ((parte1.replaceAll(" ", "").matches("-?([0-9]+\\u00BA)?([0-9]+\\u0027)?([0-9]+\\.?[0-9]*\\u0027\\u0027)?"))&&
						(parte2.replaceAll(" ", "").matches("-?([0-9]+\\u00BA)?([0-9]+\\u0027)?([0-9]+\\.?[0-9]*\\u0027\\u0027)?"))){ //coordenadas en grados minutos y segundos


					coords = new Double[]{UtilsCoordTrans.getDecimalDegrees(parte1),
							UtilsCoordTrans.getDecimalDegrees(parte2)};
					epsg = "EPSG:4258";
					try{
						coords = UtilsCoordTrans.transform(properties,coords,epsg,"EPSG:25830");
					}
					catch(Exception ex){
						logger.error("No se pudieron transformar las coordenadas", ex);
					}

				}

				else {
					if (parte2.matches(".*[0-9]+[ ]?\\([0-9]+\\)")){


						huso = parte2.substring(parte2.indexOf("(")+1,parte2.indexOf(")"));
						parte2=parte2.substring(0, parte2.indexOf("(")-1).trim();

					}
					if (esReal(parte1)&&esReal(parte2)){  // son dos n�meros separados por coma
						coords = new Double[]{Double.parseDouble(parte1),Double.parseDouble(parte2)};
						if ((!huso.equalsIgnoreCase("30"))&&(!huso.equalsIgnoreCase("25830"))){
							if (huso.length() ==2)
							{
								huso = "258"+huso;
							}
							epsg = "EPSG:"+huso;
							try{
								coords = UtilsCoordTrans.transform(properties,coords,epsg,"EPSG:25830");
							}
							catch(Exception ex){
								logger.error("No se pudieron transformar las coordenadas", ex);
							}
						}
						else{
							if ((coords[0]<=180)&&
									(coords[1]<=90)){
								epsg = "EPSG:4258";
								try{
									coords = UtilsCoordTrans.transform(properties,coords,epsg,"EPSG:25830");
								}
								catch(Exception ex){
									logger.error("No se pudieron transformar las coordenadas", ex);
								}
							}
						}
					}

				}
				if (coords!=null){
					if ((coords[0]>=571580)&&(coords[0]<=812351)&&
							(coords[1]>=4412223)&&
							(coords[1]<=4756639)){

						resultado.add( new Resultado("Coordenadas "+epsg,1,texto.replaceAll(separador, ":")+"#"+coords[0]+":"+coords[1]));
					}
					else{
						resultado.add( new Resultado("Coordenadas",0,null));
					}
					return resultado;			
				}
				else{
					resultado.add( new Resultado("Coordenadas",0,null));
				}
				return resultado;	
			}
		}
		else if (empiezaPorNumero(texto)){  // empieza por n�mero y no tiene comas
			texto = texto.replaceAll(" ", "");  // eliminar todos los espacios en blanco
			if ((numeroApariciones(texto,":")==5)||(numeroApariciones(texto,"-")==5)){ // podr�a ser parcela SIGPAC
				String refcat = formatRefCat(texto);

				if (!refcat.equalsIgnoreCase(texto)){

					// intento la busqueda de nuevo, a ver si es SIGPAC
					return buscar(refcat,beginRecord,maxRecords,maxResults,bbox);
				}
				return resultado;
			}
			else if (texto.length()==14){  // parcela catastral

				Resultado	parcela= doDBSearch(texto.toUpperCase(), properties.getProperty("CATASTRO_RUS_FTTYPE"),
						ATTR_NOMBRE,inicio,total,max,properties.getProperty("CATASTRO_RUS_DB","conexSSS_carto"),false);

				resultado.add(parcela); 
				return resultado;
			}
			else if (texto.toUpperCase().startsWith("30T")||texto.toUpperCase().startsWith("31T")){
				// Cuadr�cula UTM 10 Km
				texto = texto.toUpperCase();
				resultado.add(doDBSearch(texto, properties.getProperty("UTM10K_FTTYPE"),
						ATTR_CODIGO,inicio,total,max,properties.getProperty("UTM10K_DB","conexSSS_carto"),false));
				return resultado;
			}
			else{
				StringTokenizer token = new StringTokenizer(texto,"-");
				if (token.countTokens() == 3){  
					String parte1 = token.nextToken().trim();
					if (esEntero(parte1)){
						String parte2 = token.nextToken().trim();
						if (esEntero(parte2)){  // dos n�meros separados por -
							//Hoja 5000
							resultado.add(doDBSearch(texto, properties.getProperty("HOJA5000_FTTYPE"),
									ATTR_CODIGO,inicio,total,max,properties.getProperty("HOJA5000_DB","conexSSS_carto"),false));
							return resultado;
						}
						/*else{ // un n�mero seguido de - y una cadena de texto

							resultado.add( new Resultado("hoja 25000",0,null));
							return resultado;
						}*/
					}
				}

			}
		}
		else if(esCodigo(texto.toUpperCase(),"PORN")){ // EJEMPLO PORN108
			resultado.add( doDBSearch(texto.toUpperCase(), properties.getProperty("PORN_FTTYPE"),
					ATTR_CODIGO,inicio,total,max,properties.getProperty("PORN_DB","conexSSS_carto"),false));
			return resultado;

		}
		else if(esCodigo(texto.toUpperCase(),"ENP")){// EJEMPLO  ENP203
			resultado.add( doDBSearch(texto.toUpperCase(), properties.getProperty("ENP_FTTYPE"),
					ATTR_CODIGO,inicio,total,max,properties.getProperty("ENP_DB","conexSSS_carto"),false));
			return resultado;
		}
		else if(esCodigo(texto.toUpperCase(),"HM")){// EJEMPLO HM240266
			resultado.add( doDBSearch(texto.toUpperCase(), properties.getProperty("HUMEDAL_FTTYPE"),
					ATTR_CODIGO,inicio,total,max,properties.getProperty("HUMEDAL_DB","conexSSS_carto"),false));
			return resultado;
		}
		else if(esCodigo(texto.toUpperCase(),"ES")){ //  "LICS/ZEPAS/ZECS";// EJEMPLO ES2430032  ES0000307 ES2410049
			Resultado lics = doDBSearch(texto.toUpperCase(), properties.getProperty("LIC_FTTYPE"),
					ATTR_CODIGO,inicio,total,max,properties.getProperty("LIC_DB","conexSSS_carto"),false);
			resultado.add( lics);
			if (lics.count<max){
				Resultado zepas =	doDBSearch(texto.toUpperCase(), properties.getProperty("ZEPA_FTTYPE"),
						ATTR_CODIGO,Math.max(1,inicio-lics.count),Math.min(total,total-lics.count+inicio-1),max-lics.count,properties.getProperty("ZEPA_DB","conexSSS_carto"),false);
				resultado.add( zepas);
				if ((lics.count+zepas.count)<max){
					resultado.add(			doDBSearch(texto.toUpperCase(), properties.getProperty("ZECS_FTTYPE"),
							ATTR_CODIGO,Math.max(1,inicio-lics.count-zepas.count),Math.min(total,total-lics.count - zepas.count+inicio-1),max-lics.count - zepas.count,properties.getProperty("ZECS_DB","conexSSS_carto"),false));

				}
			}
			return resultado;

		}
		else if(esCodigo(texto.toUpperCase(),"H")||esCodigo(texto.toUpperCase(),"T")||esCodigo(texto.toUpperCase(),"Z")){// COTO o MONTE
			Resultado montes =  doDBSearch(texto.toUpperCase(), properties.getProperty("MONTE_FTTYPE"),
					ATTR_NOMBRE,inicio,total,max,properties.getProperty("MONTE_DB","conexSSS_carto"),false);
			resultado.add(montes);
			if (montes.count<max){
				Resultado cotos =  doDBSearch(texto.toUpperCase(), properties.getProperty("COTO_FTTYPE"),
						ATTR_NOMBRE,Math.max(1,inicio-montes.count),Math.min(total,total-montes.count+inicio-1),max-montes.count,properties.getProperty("COTO_DB","conexSSS_carto"),false);
				resultado.add(cotos);
			}
			return resultado;
		}
		else {
			if ((texto.matches(".+ [0-9]+"))&&(numeroApariciones(texto,",")==0)){ // cadena de texto terminada en n�mero y sin comas
				// portal en callejero
				String layer = properties.getProperty("CALLEJERO_PORTAL_FTTYPE");
				// Devuelve los resultados de buscar la calle, pero indicando en el tipo de feature "portal" para que el cliente sepa
				// que lo que quer�a el usuario es buscar un portal (ser� la segunda parte de la b�squeda cuando el usuario seleccione la calle en los resultados)
				String txt_buscar = getFonetica(texto.substring(0, texto.lastIndexOf(" ")).trim());
				//String attr = properties.getProperty("CALLEJERO_FIELD");
				String texto_fon = getFonetica(texto.trim());
				Resultado zonaedu =doDBSearch(texto_fon, properties.getProperty("ZONAEDU_FTTYPE"),
						ATTR_FONETICA,inicio,total,max,properties.getProperty("ZONAEDU_DB","conexSSS_carto"),false);

				resultado.add(zonaedu);
				int count = zonaedu.count;
				if (count < max){
					Resultado portal =	 doDBSearch(txt_buscar, layer,
							ATTR_FONETICA,Math.max(1,inicio-count),
							Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),false);
					String tipo = properties.getProperty("CALLEJERO_PORTAL_FTTYPE")+"_"+texto.substring(texto.lastIndexOf(" ")).trim();
					portal.type = tipo;
					count = count+portal.count;
					resultado.add(portal);
					if (count < max){
						Resultado catMinero =doDBSearch(texto_fon, properties.getProperty("CATMIN_FTTYPE"),
								ATTR_FONETICA,Math.max(1,inicio-count),
								Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CATMIN_DB","conexSSS_carto"),false);
						count = count+catMinero.count;
						resultado.add(catMinero );
						if (count < max){

							zonaedu =doDBSearch(texto_fon, properties.getProperty("ZONAEDU_FTTYPE"),
									ATTR_FONETICA,Math.max(1,inicio-count),
									Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZONAEDU_DB","conexSSS_carto"),true);
							count = count+zonaedu.count;
							resultado.add(zonaedu );
							if (count < max){
								portal =	 doDBSearch(txt_buscar, layer,
										ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),true);
								portal.type = tipo;
								resultado.add(portal);
								count = count+portal.count;
								if (count < max){

									catMinero =doDBSearch(texto_fon, properties.getProperty("CATMIN_FTTYPE"),
											ATTR_FONETICA,Math.max(1,inicio-count),
											Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CATMIN_DB","conexSSS_carto"),true);
									count = count+catMinero.count;
									resultado.add(catMinero );

								}
							}
						}

					}

				}
				return resultado;
			}
			else if (numeroApariciones(texto,",")==0){ // cadena de texto sin ,
				//calle, localidad, municipio, comarca, toponimo
				String texto_fon = getFonetica(texto);
				int count = 0;
				Resultado localidades =doDBSearch(texto_fon, properties.getProperty("LOCALIDAD_FTTYPE"),
						ATTR_FONETICA,inicio,total,max,properties.getProperty("LOCALIDAD_DB","conexSSS_carto"),false);
				resultado.add(localidades );
				count += localidades.count;

				if (count < max){
					Resultado munis =doDBSearch(texto_fon, properties.getProperty("MUNICIPIO_FTTYPE"),
							ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("MUNICIPIO_DB","conexSSS_carto"),false);
					resultado.add(munis );
					count += munis.count;
					if (count < max){
						Resultado calles = doDBSearch(texto_fon, properties.getProperty("CALLEJERO_FTTYPE"),
								ATTR_FONETICA,Math.max(1,inicio-count),
								Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_DB","conexSSS_carto"),false);
						resultado.add(calles );
						count += calles.count;
						if (count < max){
							Resultado comarcas =doDBSearch(texto_fon, properties.getProperty("COMARCA_FTTYPE"),
									ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("COMARCA_DB","conexSSS_carto"),false);
							resultado.add(comarcas );
							count += comarcas.count;
							if (count < max){
								Resultado cenedu =doDBSearch(texto_fon, properties.getProperty("CENEDU_FTTYPE"),
										ATTR_FONETICA,Math.max(1,inicio-count),
										Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CENEDU_DB","conexSSS_carto"),false);
								resultado.add(cenedu );
								count += cenedu.count;
								if (count < max){
									Resultado zonaedu =doDBSearch(texto_fon, properties.getProperty("ZONAEDU_FTTYPE"),
											ATTR_FONETICA,Math.max(1,inicio-count),
											Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZONAEDU_DB","conexSSS_carto"),false);
									resultado.add(zonaedu );
									count += zonaedu.count;
									if (count < max){
										Resultado inst_san =doDBSearch(texto_fon, properties.getProperty("INSTSAN_FTTYPE"),
												ATTR_FONETICA,Math.max(1,inicio-count),
												Math.min(total,total-count+inicio-1),max-count,properties.getProperty("INSTSAN_DB","conexSSS_carto"),false);
										resultado.add(inst_san );
										count += inst_san.count;
									
												if (count < max){
													Resultado zbs =doDBSearch(texto_fon, properties.getProperty("ZBS_FTTYPE"),
															ATTR_FONETICA,Math.max(1,inicio-count),
															Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZBS_DB","conexSSS_carto"),false);
													resultado.add(zbs );
													count += zbs.count;
													if (count < max){
														Resultado secsan =doDBSearch(texto_fon, properties.getProperty("SECSAN_FTTYPE"),
																ATTR_FONETICA,Math.max(1,inicio-count),
																Math.min(total,total-count+inicio-1),max-count,properties.getProperty("SECSAN_DB","conexSSS_carto"),false);
														resultado.add(secsan );
														count += secsan.count;
														if (count < max){
															Resultado catMinero =doDBSearch(texto_fon, properties.getProperty("CATMIN_FTTYPE"),
																	ATTR_FONETICA,Math.max(1,inicio-count),
																	Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CATMIN_DB","conexSSS_carto"),false);
															resultado.add(catMinero );
															count += catMinero.count;
															if (count < max){
																Resultado catMinero_like =doDBSearch(texto_fon, properties.getProperty("CATMIN_FTTYPE"),
																		ATTR_FONETICA,Math.max(1,inicio-count),
																		Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CATMIN_DB","conexSSS_carto"),true);
																resultado.add(catMinero_like );
																count += catMinero_like.count;
																if (count < max){
																	Resultado porn =doDBSearch(texto_fon, properties.getProperty("PORN_FTTYPE"),
																			ATTR_FONETICA,Math.max(1,inicio-count),
																			Math.min(total,total-count+inicio-1),max-count,properties.getProperty("PORN_DB","conexSSS_carto"),false);
																	resultado.add(porn );
																	count += porn.count;
																	if (count < max){
																		Resultado enp =doDBSearch(texto_fon, properties.getProperty("ENP_FTTYPE"),
																				ATTR_FONETICA,Math.max(1,inicio-count),
																				Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ENP_DB","conexSSS_carto"),false);
																		resultado.add(enp );
																		count += enp.count;
																		if (count < max){
																			Resultado humedales =doDBSearch(texto_fon, properties.getProperty("HUMEDAL_FTTYPE"),
																					ATTR_FONETICA,Math.max(1,inicio-count),
																					Math.min(total,total-count+inicio-1),max-count,properties.getProperty("HUMEDAL_DB","conexSSS_carto"),false);
																			resultado.add(humedales );
																			count += humedales.count;
																			if (count < max){
																				Resultado lics =doDBSearch(texto_fon, properties.getProperty("LIC_FTTYPE"),
																						ATTR_FONETICA,Math.max(1,inicio-count),
																						Math.min(total,total-count+inicio-1),max-count,properties.getProperty("LIC_DB","conexSSS_carto"),false);
																				resultado.add(lics );
																				count += lics.count;
																				if (count < max){
																					Resultado zepas =doDBSearch(texto_fon, properties.getProperty("ZEPA_FTTYPE"),
																							ATTR_FONETICA,Math.max(1,inicio-count),
																							Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZEPA_DB","conexSSS_carto"),false);
																					resultado.add(zepas );
																					count += zepas.count;
																					if (count < max){
																						Resultado zecs =doDBSearch(texto_fon, properties.getProperty("ZECS_FTTYPE"),
																								ATTR_FONETICA,Math.max(1,inicio-count),
																								Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZECS_DB","conexSSS_carto"),false);
																						resultado.add(zecs );
																						count += zecs.count;
																						if (count < max){
																							Resultado vvpp =doDBSearch(texto_fon, properties.getProperty("VVPP_FTTYPE"),
																									ATTR_FONETICA,Math.max(1,inicio-count),
																									Math.min(total,total-count+inicio-1),max-count,properties.getProperty("VVPP_DB","conexSSS_carto"),false);
																							resultado.add(vvpp );
																							count += vvpp.count;

																							if (count < max){
																								logger.info("busco topo");
																								Resultado toponimos= doDBSearch(texto_fon,"toponimo",ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count, properties.getProperty("TOPO_DB","conexSSS_carto"),false);
																								resultado.add(toponimos);
																								logger.info("result topo");

																								count += toponimos.count;
																								if (count < max){
																									logger.info("busco likes");

																									Resultado localidades_like =doDBSearch(texto_fon, properties.getProperty("LOCALIDAD_FTTYPE"),
																											ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("LOCALIDAD_DB","conexSSS_carto"),true);
																									resultado.add(localidades_like );
																									count += localidades_like.count;

																									if (count < max){
																										Resultado munis_like =doDBSearch(texto_fon, properties.getProperty("MUNICIPIO_FTTYPE"),
																												ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("MUNICIPIO_DB","conexSSS_carto"),true);
																										resultado.add(munis_like );
																										count += munis_like.count;
																										if (count < max){
																											Resultado calles_like = doDBSearch(texto_fon, properties.getProperty("CALLEJERO_FTTYPE"),
																													ATTR_FONETICA,Math.max(1,inicio-count),
																													Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_DB","conexSSS_carto"),true);
																											resultado.add(calles_like );
																											count +=calles_like.count;
																											if (count < max){

																												Resultado comarcas_like =doDBSearch(texto_fon, properties.getProperty("COMARCA_FTTYPE"),
																														ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("COMARCA_DB","conexSSS_carto"),true);
																												resultado.add(comarcas_like );
																												count += comarcas_like.count;
																												if (count < max){
																													Resultado cenedu_like =doDBSearch(texto_fon, properties.getProperty("CENEDU_FTTYPE"),
																															ATTR_FONETICA,Math.max(1,inicio-count),
																															Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CENEDU_DB","conexSSS_carto"),true);
																													resultado.add(cenedu_like );
																													count += cenedu_like.count;
																													if (count < max){
																														Resultado zonaedu_like =doDBSearch(texto_fon, properties.getProperty("ZONAEDU_FTTYPE"),
																																ATTR_FONETICA,Math.max(1,inicio-count),
																																Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZONAEDU_DB","conexSSS_carto"),true);
																														resultado.add(zonaedu_like );
																														count += zonaedu_like.count;
																														if (count < max){
																															Resultado instsan_like =doDBSearch(texto_fon, properties.getProperty("INSTSAN_FTTYPE"),
																																	ATTR_FONETICA,Math.max(1,inicio-count),
																																	Math.min(total,total-count+inicio-1),max-count,properties.getProperty("INSTSAN_DB","conexSSS_carto"),true);
																															resultado.add(instsan_like );
																															count += instsan_like.count;
																														
																																	if (count < max){
																																		Resultado zbs_like =doDBSearch(texto_fon, properties.getProperty("ZBS_FTTYPE"),
																																				ATTR_FONETICA,Math.max(1,inicio-count),
																																				Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZBS_DB","conexSSS_carto"),true);
																																		resultado.add(zbs_like );
																																		count += zbs_like.count;
																																		if (count < max){
																																			Resultado secsan_like =doDBSearch(texto_fon, properties.getProperty("SECSAN_FTTYPE"),
																																					ATTR_FONETICA,Math.max(1,inicio-count),
																																					Math.min(total,total-count+inicio-1),max-count,properties.getProperty("SECSAN_DB","conexSSS_carto"),true);
																																			resultado.add(secsan_like );
																																			count += secsan_like.count;
																																			if (count < max){
																																				Resultado porn_like =doDBSearch(texto_fon, properties.getProperty("PORN_FTTYPE"),
																																						ATTR_FONETICA,Math.max(1,inicio-count),
																																						Math.min(total,total-count+inicio-1),max-count,properties.getProperty("PORN_DB","conexSSS_carto"),true);
																																				resultado.add(porn_like );
																																				count += porn_like.count;
																																				if (count < max){
																																					Resultado enp_like =doDBSearch(texto_fon, properties.getProperty("ENP_FTTYPE"),
																																							ATTR_FONETICA,Math.max(1,inicio-count),
																																							Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ENP_DB","conexSSS_carto"),true);
																																					resultado.add(enp_like );
																																					count += enp_like.count;
																																					if (count < max){
																																						Resultado humedales_like =doDBSearch(texto_fon, properties.getProperty("HUMEDAL_FTTYPE"),
																																								ATTR_FONETICA,Math.max(1,inicio-count),
																																								Math.min(total,total-count+inicio-1),max-count,properties.getProperty("HUMEDAL_DB","conexSSS_carto"),true);
																																						resultado.add(humedales_like );
																																						count += humedales_like.count;
																																						if (count < max){
																																							Resultado lics_like =doDBSearch(texto_fon, properties.getProperty("LIC_FTTYPE"),
																																									ATTR_FONETICA,Math.max(1,inicio-count),
																																									Math.min(total,total-count+inicio-1),max-count,properties.getProperty("LIC_DB","conexSSS_carto"),true);
																																							resultado.add(lics_like );
																																							count += lics_like.count;
																																							if (count < max){
																																								Resultado zepas_like =doDBSearch(texto_fon, properties.getProperty("ZEPA_FTTYPE"),
																																										ATTR_FONETICA,Math.max(1,inicio-count),
																																										Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZEPA_DB","conexSSS_carto"),true);
																																								resultado.add(zepas_like );
																																								count += zepas_like.count;
																																								if (count < max){
																																									Resultado zecs_like =doDBSearch(texto_fon, properties.getProperty("ZECS_FTTYPE"),
																																											ATTR_FONETICA,Math.max(1,inicio-count),
																																											Math.min(total,total-count+inicio-1),max-count,properties.getProperty("ZECS_DB","conexSSS_carto"),true);
																																									resultado.add(zecs_like );
																																									count += zecs_like.count;
																																									if (count < max){
																																										Resultado vvpp_like =doDBSearch(texto_fon, properties.getProperty("VVPP_FTTYPE"),
																																												ATTR_FONETICA,Math.max(1,inicio-count),
																																												Math.min(total,total-count+inicio-1),max-count,properties.getProperty("VVPP_DB","conexSSS_carto"),true);
																																										resultado.add(vvpp_like );
																																										count += vvpp_like.count;

																																										if (count < max){
																																											//	logger.info("busco topolike");
																																											Resultado toponimos_like= doDBSearch(texto_fon,"toponimo",ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1), max-count,properties.getProperty("TOPO_DB","conexSSS_carto"),true);
																																											resultado.add(toponimos_like);
																																											//	logger.info("result topo");
																																										}
																																									}
																																								}
																																							}
																																						}
																																					}
																																				}
																																			}
																																		}
																																	}
																																}
																															}
																														}
																												
																											
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				return resultado;
			}
			else if (numeroApariciones(texto,",")==1){  // dos cadenas de texto separadas por coma
				// calle, municipio // localidad, municipio // toponimo, municipio
				StringTokenizer tokenizer = new StringTokenizer(texto,",");
				String token1=tokenizer.nextToken();
				String token2="";
				if (tokenizer.hasMoreTokens()){ // por si no hay nada detrás de la coma
					token2=tokenizer.nextToken();  // deber�a ser el municipio
				}
				if (token1.trim().matches(".+[0-9]+")){  // es portal
					String layer = properties.getProperty("CALLEJERO_PORTAL_FTTYPE");
					String[] txts = new String[]{getFonetica(token1.substring(0, token1.lastIndexOf(" ")).trim()), getFonetica(token2)};
					String[] attrs_loc = new String[]{ATTR_FONETICA,ATTR_LOCALIDAD};
					String[] attrs_muni = new String[]{ATTR_FONETICA,ATTR_MUNICIPIO};
					String tipo =  properties.getProperty("CALLEJERO_PORTAL_FTTYPE")+"_"+token1.substring(token1.lastIndexOf(" ")).trim();
					Resultado respuesta = doDBSearch(txts, layer,attrs_loc, null
							,inicio,total,max,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),false);
					respuesta.type =tipo;
					resultado.add(respuesta);
					int count = respuesta.count;
					if (count<max){
						respuesta = doDBSearch(txts, layer,attrs_muni, new String[]{null,ATTR_LOCALIDAD}
								,Math.max(1,inicio-count),
								Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),false);

						respuesta.type =tipo;
						resultado.add(respuesta);
						count = count +respuesta.count;
						if (count < max){
							respuesta = doDBSearch(txts, layer,
									attrs_loc,null,Math.max(1,inicio-count),
									Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),true);
							respuesta.type =tipo;
							resultado.add(respuesta);
							count = count +respuesta.count;
							if (count< max){
								respuesta = doDBSearch(txts, layer,attrs_muni, new String[]{null,ATTR_LOCALIDAD}, Math.max(1,inicio-respuesta.count),
										Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),true);
								respuesta.type =tipo;
								resultado.add(respuesta);
							}

						}
					}
					return resultado;

				}

				else{
					token2=token2.trim();
					boolean esMuniINE=(esEntero(token2)&&(token2.length()==5));
					token1=getFonetica(token1);
					if (!esMuniINE){
						token2=getFonetica(token2);
					}

					// calle, localidad o toponimos en municipio
					int count = 0;
					Resultado localidades =doDBSearch(new String[]{token1,token2}, properties.getProperty("LOCALIDAD_FTTYPE"),
							new String[]{ATTR_FONETICA,(esMuniINE ? ATTR_MUNI_INE:ATTR_MUNICIPIO)},null,inicio,total,max,properties.getProperty("LOCALIDAD_PORTAL_DB","conexSSS_carto"),false);
					resultado.add(localidades);
					count += localidades.count;
					logger.info("count="+count);
					if (count < max){
						Resultado calles;
						if (esMuniINE){
							calles= doDBSearch(token1, properties.getProperty("CALLEJERO_FTTYPE"),
									ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_DB","conexSSS_carto"),false);
							count += calles.count;
						}
						else{
							calles= doDBSearch(new String[]{token1,token2}, properties.getProperty("CALLEJERO_FTTYPE"),
									new String[]{ATTR_FONETICA,ATTR_LOCALIDAD},null,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),false);
							resultado.add(calles);
							count += calles.count;
							logger.info("count="+count);
							if (count < max){
								calles= doDBSearch(new String[]{token1,token2}, properties.getProperty("CALLEJERO_FTTYPE"),
										new String[]{ATTR_FONETICA,ATTR_MUNICIPIO}, new String[]{null,ATTR_LOCALIDAD},Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),false);

								resultado.add(calles);
								count += calles.count;
								logger.info("count="+count);
							}
						}

						if (count < max){
							Resultado toponimos=doDBSearch(new String[]{token1,token2},"toponimo",new String[]{ATTR_FONETICA,(esMuniINE ? ATTR_MUNI_INE:ATTR_MUNICIPIO)},null,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count, properties.getProperty("TOPO_DB","conexSSS_carto"),false);

							logger.info("resultado toponimos");
							resultado.add(toponimos);

							count += toponimos.count;
							logger.info("count="+count);
							if (count < max){

								Resultado localidades_like =doDBSearch(new String[]{token1,token2}, properties.getProperty("LOCALIDAD_FTTYPE"),
										new String[]{ATTR_FONETICA,(esMuniINE ? ATTR_MUNI_INE:ATTR_MUNICIPIO)},null,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("LOCALIDAD_DB","conexSSS_carto"),true);
								resultado.add(localidades_like);
								count += localidades_like.count;
								if (count < max){
									Resultado calles_like;
									if (esMuniINE){
										calles_like = doDBSearch(token1, properties.getProperty("CALLEJERO_FTTYPE"),
												ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_DB","conexSSS_carto"),true);
										count += calles_like.count;
									}
									else{
										calles_like = doDBSearch(new String[]{token1,token2}, properties.getProperty("CALLEJERO_FTTYPE"),
												new String[]{ATTR_FONETICA,ATTR_LOCALIDAD},null,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),true);
										resultado.add(calles_like);
										count += calles_like.count;
										if (count < max){
											calles_like = doDBSearch(new String[]{token1,token2}, properties.getProperty("CALLEJERO_FTTYPE"),
													new String[]{ATTR_FONETICA,ATTR_MUNICIPIO}, new String[]{null,ATTR_LOCALIDAD},Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count,properties.getProperty("CALLEJERO_PORTAL_DB","conexSSS_carto"),true);
											resultado.add(calles_like);
											count += calles_like.count;
										}
									}

									if (count < max){
										Resultado toponimos_like= doDBSearch(new String[]{token1,token2},"toponimo",new String[]{ATTR_FONETICA,(esMuniINE ? ATTR_MUNI_INE:ATTR_MUNICIPIO)},null,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count, properties.getProperty("TOPO_DB","conexSSS_carto"),true);

										resultado.add(toponimos_like);
									}
								}
							}
						}
					}

					return resultado;
				}


			}
			else { // la �ltima opci�n es buscar por top�nimo
				logger.info("Buscar toponimo");
				String texto_fon = getFonetica(texto);
				Resultado toponimos= doDBSearch(texto_fon,"toponimo",ATTR_FONETICA,inicio,total,max, properties.getProperty("TOPO_DB","conexSSS_carto"),false);
				logger.info("resultado topo");
				resultado.add(toponimos);
				int count = 0;

				count += toponimos.count;

				if (count < max){
					logger.info("Buscar toponimo parecido");
					Resultado toponimos_like= doDBSearch(texto_fon,"toponimo",ATTR_FONETICA,Math.max(1,inicio-count),Math.min(total,total-count+inicio-1),max-count, properties.getProperty("TOPO_DB","conexSSS_carto"),true);
					resultado.add(toponimos_like);
				}
				logger.info("return");
				return resultado;
			}
		}
		logger.info("return");
		return resultado;
	}

	public static Resultado doNominatimSearch(String text,int max,int begin){
		logger.info("Max nominatim "+max);
		String url = properties.getProperty("NOMINATIM_URL").replaceAll("<<TEXT>>",text.replaceAll(" ", "+").replaceAll("ny", "n"));
		String response = doSearch(url, new Integer(properties.getProperty("NOMINATIM_GEONAMES_TIMEOUT","0")));
		if (response.length()==0){
			return new Resultado("Geonames",0,null);
		}
		String [] trozos = response.split("\\},\\{");
		if ((trozos.length==1)&&(trozos[0].length()<5)){//sin resultados
			trozos = new String[0];
		}
		String respuestaStr=null;

		StringBuffer respuesta=new StringBuffer();
		for (int i=Math.max(0,begin-1); i<Math.min(trozos.length,max);i++){
			if (respuesta.length()==0){
				if  (i>0){
					respuesta.append("[{");
				}
			}
			else{
				respuesta.append(",{");
			}
			respuesta.append(trozos[i]+"}");
		}
		if (respuesta.length()>0){
			respuestaStr=respuesta.toString();
		}
		return new Resultado("Nominatim",trozos.length,respuestaStr);
	}

	public static Resultado doGeonamesSearch(String text,int max,int init){
		logger.info("Max geonames "+max);
		String url = properties.getProperty("GEONAMES_URL").replaceAll("<<BEGIN_POS>>",new Integer(Math.max(0,init)).toString()).replaceAll("<<MAX_RESULTS>>",new Integer(Math.max(1,max)).toString()).replaceAll("<<TEXT>>",text.replaceAll(" ", "+").replaceAll("ny", "n"));
		String response = doSearch(url, new Integer(properties.getProperty("NOMINATIM_GEONAMES_TIMEOUT","0")));
		if (response.length()==0){
			return new Resultado("Geonames",0,null);
		}
		String count = response.substring(response.indexOf("\"totalResultsCount\":")+"\"totalResultsCount\":".length(),
				response.indexOf(","));
		return new Resultado("Geonames",count,(max<=0?null:response));
	}
	/*public static ArrayList<Resultado> doWFSGSearch(String text, String ine, String text_fonetica,int begin_pos, int max_features, int max_results, String bbox, boolean like, boolean searchExternal, boolean searchFonetica, boolean onlyGeorreferenced){
		ArrayList<Resultado> resultado = new ArrayList<Resultado>();

		// coincidencia exacta
		String equal_condition="";
		if (searchFonetica){
			equal_condition= properties.getProperty("WFSG_EQUAL_COMPARISON").replaceAll("<<TEXT>>","#"+text_fonetica.replaceAll(" ", "#")+"#").replaceAll("<<FIELD>>", "busqueda");
		}
		else{
			equal_condition= properties.getProperty("WFSG_EQUAL_COMPARISON").replaceAll("<<TEXT>>",text).replaceAll("<<FIELD>>", "nombre");
		}
		if (ine != null){
			equal_condition = "<ogc:And>"+equal_condition+properties.getProperty("WFSG_INE_COMPARISON").replaceAll("<<TEXT>>",ine)+"</ogc:And>";
		}
		String bbox_condition = null;
		if ((bbox != null)&&(bbox.length()>0)){
			bbox_condition = properties.getProperty("WFSG_BBOX_FILTER").replaceAll("<<BBOX>>",bbox);

		}
		if (!like){
			logger.info("exacta");
			Resultado exactSearch = doWFSGSearchCond("nombreExacto",(bbox_condition != null ? "<ogc:And>"+bbox_condition+equal_condition+"</ogc:And>":equal_condition),begin_pos, max_features, max_results, bbox,onlyGeorreferenced);
			resultado.add(exactSearch);
			//if ((exactSearch.count>0)) //Si hay resultado en la búsqueda exacta no buscar más 
			return resultado;
		}



		StringTokenizer tokenizer = new StringTokenizer(text_fonetica," ");
		Resultado andSearch = null;
		String filter = "";
		String condition = properties.getProperty("WFSG_LIKE_COMPARISON");

		if (searchFonetica){
			condition = condition.replaceAll("<<FIELD>>", "busqueda");
		}
		else{
			condition = condition.replaceAll("<<FIELD>>", "nombre");
		}
		int andCount = 0;
		if (tokenizer.countTokens()>1){  // el texto buscado se compone de varias palabras

			while (tokenizer.hasMoreElements()){
				if (searchFonetica){
					filter = (filter.length()>0? " <ogc:And>":"")+filter + condition.replaceAll("<<TEXT>>", "*#"+tokenizer.nextToken()+ "#*")+(filter.length()>0? " </ogc:And>":"");
				}
				else{
					filter = (filter.length()>0? " <ogc:And>":"")+filter + condition.replaceAll("<<TEXT>>", "*"+tokenizer.nextToken()+ "*")+(filter.length()>0? " </ogc:And>":"");
				}

			}

			// Filtro para b�squeda por todas las palabras (y no coincidencia exacta, para no obtener resultados repetidos)
			String andFilter = "<ogc:And><ogc:Not>"+equal_condition+"</ogc:Not>"+
			(bbox_condition != null ? bbox_condition+filter:filter)+
			(ine!=null? properties.getProperty("WFSG_INE_COMPARISON").replaceAll("<<TEXT>>",ine):"")+"</ogc:And>";
			logger.info("todas");
			andSearch = doWFSGSearchCond("nombreTodas",andFilter,begin_pos, max_features, max_results, bbox,onlyGeorreferenced);
			// devolver los resultados de las 3 b�squedas

			resultado.add(andSearch);
			andCount = andSearch.count;
			if (andCount>0)  // si hay resultado por todas las palabras no buscar por alguna palabra
				return resultado;
		}

		String orFilter =null;
		if (andSearch != null){// varias palabras
			// Filtro para b�squeda por alguna palabra (y no coincidencia exacta ni todas las palabras, para no obtener resultados repetidos)
			orFilter = "<ogc:And><ogc:Not>"+equal_condition+"</ogc:Not><ogc:Not>"+filter
			+"</ogc:Not>"+(bbox_condition != null ? bbox_condition+filter.replaceAll("ogc:And>", "ogc:Or>"):filter.replaceAll("ogc:And>", "ogc:Or>"))+
			(ine!=null? properties.getProperty("WFSG_INE_COMPARISON").replaceAll("<<TEXT>>",ine):"")+"</ogc:And>";
		}
		else{
			orFilter = "<ogc:And><ogc:Not>"+equal_condition+"</ogc:Not>"+(bbox_condition != null ? bbox_condition:"")+condition.replaceAll("<<TEXT>>",(searchFonetica ?  "*#"+tokenizer.nextToken()+ "#*":"*"+tokenizer.nextToken()+ "*"))+(ine !=null ? properties.getProperty("WFSG_INE_COMPARISON").replaceAll("<<TEXT>>",ine):"")+"</ogc:And>";
		}
		// Buscar por todas las palabras
		//return exactSearch.getResult()+"##" + andSearch.getResult()+"##"+
		logger.info("alguna");
		Resultado orSearch =doWFSGSearchCond("nombreAlguna",orFilter,Math.max(1,begin_pos-  andCount ), max_features-Math.max( andCount - begin_pos+ 1,0),max_results-andCount, bbox,onlyGeorreferenced); 
		resultado.add(orSearch);
		searchExternal = searchExternal && (andCount <=0)&& (orSearch.count <=0);

		if ((searchExternal && (ine==null))&&(max_results>=begin_pos)){
			Resultado nominatim = doNominatimSearch(text,Math.max(0, Math.min(max_features, max_results)),begin_pos);
			resultado.add(nominatim);
			resultado.add(doGeonamesSearch(text,Math.max(0, Math.min(max_features, max_results)-nominatim.count),Math.max(0,begin_pos-nominatim.count-1)));
		}
		return resultado;
	}*/
	/**
	 * Busca en el gazetteer. Hace la b�squeda por coincidencia exacta, todas las palabras y alguna palabra
	 * @param text texto a buscar
	 * @param begin_pos �ndice del primer resultado a devolver
	 * @param max_features n�mero m�ximo de features.
	 * @return resultado de la b�squeda en el gazeeteer compuesta por los resultados de las 3 b�squedas restringidas (exacta, todas las palabras, alguna palabra)
	 */
	/*	public static ArrayList<Resultado> doWFSGSearch(String text, String text_fonetica,int begin_pos, int max_features,int max_results, String bbox, boolean like, boolean searchExternal){

		return doWFSGSearch( text, null, text_fonetica, begin_pos,  max_features, max_results, bbox,  like,searchExternal, true,false);

	}
	public static ArrayList<Resultado> doWFSGSearch(String text, String text_fonetica,int begin_pos, int max_features,int max_results, String bbox, boolean like, boolean searchExternal, boolean searchFonetica){

		return doWFSGSearch( text, null, text_fonetica, begin_pos,  max_features, max_results, bbox,  like,searchExternal, searchFonetica, false);

	}*/
	/**
	 * Obtiene el n�mero de resultados de una b�squeda en el gazetteer
	 * @param request petici�n para el servicio
	 * @return n�mero de resultados
	 */
	/*protected static ArrayList<String> doWFSGSearchCount(String request){
		// Configurar petici�n para que devuelva el n�mero de resultados
		String count_request = request.replaceFirst("<<RESULT>>", "propertyName=\"app:identidad\"");
		count_request = count_request.replaceFirst("<<BEGIN_POS>>", "1");
		// Se buscan nombres pero los resultados son las entidades, por lo que no puedo limitar al máximo de resultados pq igual nos quedamos cortos con las entidades
		count_request = count_request.replaceFirst("<<MAX_RESULTS>>", properties.getProperty("MAX_RESULTS"));
		// Ejecutar la b�squeda
		String count = doSearch(properties.getProperty("WFSG_URL"),count_request);

		ArrayList<String> ids = new ArrayList<String>();
		// Parsear la respuesta para obtener el n�mero de resultados
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new ByteArrayInputStream(count.getBytes()));
			doc.getDocumentElement().normalize();

			// Parsear la respuesta para obtener el n�mero de resultados

			NodeList entidades = doc.getElementsByTagName("app:identidad");

			for (int i=0; i<entidades.getLength(); i++){
				if ((entidades.item(i).getParentNode().getNodeName().equalsIgnoreCase("app:Nombres"))&&
						(!ids.contains(entidades.item(i).getFirstChild().getNodeValue()))){
					ids.add(entidades.item(i).getFirstChild().getNodeValue());
				}
			}



		}
		catch (Exception ex){
			logger.error("Error al obtener en la consulta al WFS-G "+count_request, ex);
			return ids;
		}
		return ids;

	}*/

	/**
	 * Busca en el gazetteer. 
	 * @param type tipo de b�squeda a indicar en el resultado (ser� nombreExacto, nombreTodas, nombreAlguna)
	 * @param condition filtro de b�squeda a pasar al servicio
	 * @param begin_pos �ndice del primer resultado a devolver
	 * @param max_features n�mero m�ximo de features a devolver (si es 0 o negativo no se devuelven los resultados)
	 * @return resultado de la b�squeda en el gazeeteer  
	 */
	/*protected static Resultado doWFSGSearchCond(String type, String condition, Integer begin_pos, Integer max_features,int max_results, String bbox, boolean onlyGeorreferenced){

		String request = properties.getProperty("WFSG_XML_REQUEST");
		request = request.replaceFirst("<<COMPARISON>>", condition);  // establecer el filtro

		// Obtener el numero total de resultados
		ArrayList<String> identidades=  doWFSGSearchCount(request);
		int total = identidades.size();
		if ((total==0)||(max_features<=0)||(total<begin_pos)||(max_results<begin_pos)){  // no hay resultados o no se piden los resultados
			return new Resultado(type,total,null);
		}
		request = properties.getProperty("WFSG_ENTIDADES_REQUEST");
		String comparison =  properties.getProperty("WFSG_ENTIDADES_COMPARISON");
		StringBuffer cond = new StringBuffer();
		int num_conds=0;
		for (int i=begin_pos-1;i<Math.min(begin_pos-1+Math.min(max_features,max_results),total);i++){
			cond.append(comparison.replaceFirst("<<TEXT>>",identidades.get(i)));
			num_conds++;
		}
		String filtros = (num_conds>1 ?"<ogc:Or>"+cond.toString()+"</ogc:Or>":cond.toString());
		if (onlyGeorreferenced){
			filtros ="<ogc:And>"+properties.getProperty("WFSG_COORD_FILTER")+ filtros +"</ogc:And>";
		}
		request = request.replaceFirst("<<COMPARISON>>", filtros);  // establecer el filtro

		// Configurar petici�n de los resultados
		//request = request.replaceFirst("<<RESULT>>", "");
		//request = request.replaceFirst("<<BEGIN_POS>>", begin_pos.toString());
		request = request.replaceFirst("<<MAX_RESULTS>>", Integer.toString(Math.min(max_results,max_features)));
		// Pedir los resultados al servicio
		String list = doSearch(properties.getProperty("WFSG_URL"),request);
logger.info("Respuesta:"+list);
		// Parsear la respuesta para obtener el n�mero de resultados
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new ByteArrayInputStream(list.getBytes()));
			doc.getDocumentElement().normalize();
			NodeList features = doc.getElementsByTagName("app:Entidades");
			StringBuffer results=new StringBuffer();;
			identidades= new ArrayList<String>();
			int count=0;
			for (int i=0; i<features.getLength(); i++){
				//logger.info("bucle i"+i);
				Node feature = features.item(i);
				String identidad = feature.getAttributes().getNamedItem("gml:id").toString();
				if (!identidades.contains(identidad)){
					identidades.add(identidad);
					NodeList attributes = feature.getChildNodes();
					String[] valores= new String[4];
					for (int j=0; j<attributes.getLength(); j++){
						//logger.info("bucle j"+j);
						if (attributes.item(j).getNodeName().equalsIgnoreCase("app:etiqueta")){
							valores[0]=attributes.item(j).getFirstChild().getNodeValue();
						}
						else if (attributes.item(j).getNodeName().equalsIgnoreCase("app:cmunine")){
							valores[1]=attributes.item(j).getFirstChild().getNodeValue();
						}
						else if (attributes.item(j).getNodeName().equalsIgnoreCase("gml:boundedBy")){
							NodeList corners = attributes.item(j).getFirstChild().getChildNodes();
							valores[2]=corners.item(0).getFirstChild().getNodeValue().replaceAll(" ", ":");
							if (valores[2].equalsIgnoreCase("0.0:0.0")){
								valores[2]="";
							}
							String corner2=corners.item(1).getFirstChild().getNodeValue().replaceAll(" ", ":");
							if (!corner2.equalsIgnoreCase("0.0:0.0")){
								if ((valores[2].length()>0)&&(!corner2.equalsIgnoreCase(valores[2]))){
									if (corners.item(1).getFirstChild().getNodeValue().equalsIgnoreCase("gml:lowerCorner")){
										valores[2]=corner2+":"+valores[2];
									}
									else{
										valores[2]=valores[2]+":"+corner2;

									}
								}

								else valores[2]=corner2;
							}
						}
						else if (attributes.item(j).getNodeName().equalsIgnoreCase("app:identidad")){
							valores[3]=attributes.item(j).getFirstChild().getNodeValue();
						}
					}
					count++;
					results.append(valores[0]);
					results.append("#");
					results.append(valores[1]);
					results.append("#");
					results.append(valores[2]);
					results.append("#");
					results.append(valores[3]);
					results.append("\n");
				}
			}
		//	logger.info("return");
			return new Resultado(type,(onlyGeorreferenced?count:total),results.toString());
		}
		catch (Exception ex){
			logger.error("Error al procesar la respuesta del WFS-G "+request, ex);
			return new Resultado(type,total,null);
		}


	}*/
	protected static void closeConn(ResultSet rs, Statement stmt, Connection conn){
		try{
			if (rs != null) rs.close();
			if (stmt != null) stmt.close();
			conn.close();
		}
		catch(Exception ex){

		}
	}



	/**
	 * Realiza una b�squeda en el servicio ArcIMS
	 * @param condition filtro de la b�squeda
	 * @param layer capa en la que buscar
	 * @param begin_pos �ndice del primer resultado a devolver
	 * @param max_features m�ximo n�mero de resultados a devolver 
	 * @param environment si true hacer la b�squeda en el servicio de Medio Ambiente, si no en el Visor2D
	 * @return resultado de la b�squeda compuesto (separados por coma) por el tipo de b�squeda (layer), el n�mero total de coincidencias, y los resultados tal y como los devuelve el ArcIMS
	 */
	protected static Resultado doDBSearch(String condition, String tipo, int begin_pos, int max_features, int max_results, String dbConn){
		logger.info("doDBSearch");
		if (conexError.get(dbConn)!=null){
			logger.error("No se intenta conectar a "+dbConn+" dado que ha fallado anteriormente");
			return new Resultado(tipo,0,null);
		}
		Connection conn =null;
		try{
			conn = ConexionBD.getConnection(dbConn);
		}
		catch(Exception ex){
			logger.error("No se pudo conectar con la base de datos", ex);
			conexError.put(dbConn, true);
			return new Resultado(tipo,0,null);
		}

		logger.info("Tengo conex");
		Statement sentencias=null;
		try{
			sentencias= conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		}
		catch(Exception ex){
			logger.error("Error al crear statement ", ex);
			closeConn( null,  sentencias,  conn);
			return new Resultado(tipo,0,null);
		}
		logger.info("Tengo stmt");
		ResultSet rs =null;

		int total = -1;
		String vista = "v_busquedas";
		String select = "SELECT count(*) as num";
		String finalSQL="";
		if(tipo.equalsIgnoreCase("toponimo")){
			vista =vista+"_topo";
			select = select+" FROM (select distinct gid ";
			finalSQL=finalSQL+")t";
		}
		//	logger.info("begin_pos "+begin_pos);
		if ((begin_pos >1)||(max_features != max_results)){ // resultado paginado
			String sql = select+" FROM  "+vista+" where lower(tipo)='"+tipo.toLowerCase()+"' and "+condition+" LIMIT "+Integer.toString(Math.min(max_results, Integer.parseInt(properties.getProperty("MAX_RESULTS"))))+finalSQL;
			logger.info(sql);
			try{
				rs = sentencias.executeQuery(sql);


				rs.next();
				total = rs.getInt("num");
				// Parsear respuesta para obtener el n�mero de resultados


				logger.info("Num result "+total);
			}
			catch (Exception ex){
				logger.error("Error al ejecutar SQL "+sql, ex);
				closeConn( rs,  sentencias,  conn);
				return new Resultado(tipo,0,null);
			}

			if ((total==0)||(max_features<=0)||(total<begin_pos)||(max_results<begin_pos)){  // no hay resultados o no se piden los resultados
				// no hacer la b�squeda de los resultados
				logger.info("No se busca");
				closeConn( rs,  sentencias,  conn);
				return new Resultado(tipo,total,null);
			}
		}
		String fields = "gid,nombre,municipio,c_muni_ine ";
		if(tipo.equalsIgnoreCase("toponimo")){
			fields = " DISTINCT xutm, yutm,codigo,"+fields;
		}
		String sql = "SELECT "+fields+" FROM  "+vista+" where lower(tipo)='"+tipo.toLowerCase()+"' and "+condition+" OFFSET "+ new Integer(begin_pos-1).toString()+
				" LIMIT "+Integer.toString(Math.min(max_results, max_features));
		logger.info(sql);
		StringBuffer results = new StringBuffer();
		int count=0;
		try{
			rs = sentencias.executeQuery(sql);

			while(rs.next()){
				count++;
				logger.info("Next");
				results.append(rs.getString("nombre"));
				results.append("#");
				if (rs.getString("municipio")!=null){
					results.append(rs.getString("municipio"));
				}
				results.append("#");
				if(tipo.equalsIgnoreCase("toponimo")){
					results.append(rs.getDouble("xutm"));
					results.append(":");
					results.append(rs.getDouble("yutm"));
				}
				//results.append(coordenadas);
				results.append("#");
				results.append(rs.getString("gid"));
				results.append("#");
				if (rs.getString("c_muni_ine")!=null){
					results.append(rs.getString("c_muni_ine"));
				}
				results.append("#");
				if(tipo.equalsIgnoreCase("toponimo")){
					if (rs.getString("codigo")!=null){
						results.append(rs.getString("codigo"));
					}
				}
				results.append("\n");
			}
			closeConn( rs,  sentencias,  conn);
		}
		catch (Exception ex){
			logger.error("Error al ejecutar SQL "+sql, ex);
			closeConn( rs,  sentencias,  conn);
			return new Resultado(tipo,0,null);
		}

		logger.info(results.toString());
		return new Resultado(tipo,(total>0?total:count),results.toString());
	}

	/*protected static String getArcIMSFeatureCount(String response) throws Exception{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new ByteArrayInputStream(response.getBytes()));
		doc.getDocumentElement().normalize();
		return ((Element)doc.getElementsByTagName("FEATURECOUNT").item(0)).getAttribute("count");
	}*/
	/**
	 * Obtiene el filtro para una peticion al servicio ArcIMS (comparaci�n del tip "todas las palabras")
	 * @param text cadena de texto a buscar
	 * @param attr atributo con el que comparar el texto
	 * @return Filtro que implementa la b�squeda de features en las que el atributo contiene todas las palabras del texto
	 */
	protected static String getSQLComparison (String text, String attr, boolean like, boolean onlyLike, String notAttr){
		logger.info("comparison");
		/*if (text.toLowerCase().equals(text.toUpperCase()) ){
			comparison = properties.getProperty("ARCIMS_NUM_COMPARISON");
		}*/
		String equal_condition =  " lower("+attr+")='"+text.toLowerCase()+"'";
		if (notAttr != null){
			equal_condition+=  " and lower("+notAttr+")<>'"+text.toLowerCase()+"'";
		}
		if (like){


			String filter = "";
			// A�adir la condici�n para cada una de las palabras del texto
			StringTokenizer tokenizer = new StringTokenizer(text," ");
			while (tokenizer.hasMoreElements()){
				filter = filter + (filter.length()>0? " AND ":"")+" lower("+attr+") LIKE '%"+tokenizer.nextToken().toLowerCase()+"%'";
				if (notAttr != null){
					filter+=  " and lower("+notAttr+")<>'"+text.toLowerCase()+"' and lower("+notAttr+") not like'%"+text.toLowerCase()+"%'" ;
				}
			}
			if (onlyLike){
				return filter;
			}
			else{
				return filter+ " AND NOT ("+equal_condition+")";
			}
		}
		else{
			return equal_condition;
		}
	}

	protected static String getSQLComparison (String text, String attr, boolean like, boolean onlyLike){
		return getSQLComparison( text,  attr,  like, onlyLike, null);
	}
	protected static String getSQLComparison(String text, String attr, boolean like){
		return getSQLComparison( text,  attr,  like, false);
	}

	protected static String getSQLComparison(String text, String attr, boolean like, String notAttr){
		return getSQLComparison( text,  attr,  like, false, notAttr);
	}

	/**
	 * Realiza una b�squeda en el servicio ArcIMS
	 * @param text lista de cadenas a comparar con los atributos de attr (en el mismo orden)
	 * @param layer capa en la que buscar
	 * @param attr lista de atributos con la que comparar los textos
	 * @param begin_pos �ndice del primer resultado a devolver
	 * @param max_features m�ximo n�mero de resultados a devolver 
	 * @param environment si true hacer la b�squeda en el servicio de Medio Ambiente, si no en el Visor2D
	 * @return resultado de la b�squeda compuesto (separados por coma) por el tipo de b�squeda (layer), el n�mero total de coincidencias, y los resultados tal y como los devuelve el ArcIMS
	 */
	public static Resultado doDBSearch(String[] text, String layer, String[] attr, String[] notAttr, int begin_pos, int max_features, int max_results, String dbConn, boolean like){
		//logger.info("DBSearch");
		// Construir el filtro anidando las comparaciones de los distintos atributos
		String filter="";
		for (int i=0; i<text.length; i++){
			
			filter = filter + (filter.length()>0? " AND ":"")+getSQLComparison(text[i],attr[i],like, true,(notAttr != null ? notAttr[i]:null));
		}
		//logger.info("filter: "+filter);
		String exact_filter="";
		if (like){
			for (int i=0; i<text.length; i++){
				exact_filter = exact_filter + (exact_filter.length()>0? " AND ":"")+getSQLComparison(text[i],attr[i],false);
			}
		}
		if (exact_filter.length()>0){
		//	logger.info(exact_filter);
			filter += " AND NOT ("+exact_filter+")";
		}
		//logger.info("filterfinal:"+filter);
		// ejecutar la b�squeda
		return doDBSearch(filter,layer,begin_pos, max_features,max_results,dbConn);

	}

	/**
	 * Realiza una b�squeda en el servicio ArcIMS
	 * @param text cadena de texto a buscar
	 * @param layer capa en la que buscar
	 * @param attr atributo en el que buscar la cadena de texto
	 * @param begin_pos �ndice del primer resultado a devolver
	 * @param max_features m�ximo n�mero de resultados a devolver 
	 * @param environment si true hacer la b�squeda en el servicio de Medio Ambiente, si no en el Visor2D
	 * @return resultado de la b�squeda compuesto (separados por coma) por el tipo de b�squeda (layer), el n�mero total de coincidencias, y los resultados tal y como los devuelve el ArcIMS
	 */
	public static Resultado doDBSearch(String text, String tipo, String attr, int begin_pos, int max_features, int max_results, String dbConn,boolean like){



		return doDBSearch(getSQLComparison(text,attr,like),tipo,begin_pos, max_features,max_results,dbConn);
	}

	protected static String doSearch(String parametrized_url){
		return doSearch(parametrized_url,0);
	}
	protected static String doSearch(String parametrized_url,int timeout){
		try{
			URL url = new URL(parametrized_url);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);

			connection.setConnectTimeout(timeout);

			logger.info(url);

			/*connection.setRequestProperty("Content-Type", "text/xml"); 
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(request.getBytes().length));
			 */
			//connection.setRequestProperty("charset", "utf-8");
			connection.connect();
			/*	OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			wr.write(request);
			wr.flush();*/

			//read the result from the server
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				sb.append(line + '\n');
			}


			connection.disconnect();

			return sb.toString();
		}
		catch(MalformedURLException ex){
			logger.error("Error en la consulta "+parametrized_url, ex);
		}
		catch(java.net.SocketTimeoutException ex){
			logger.error("Tiempo máximo de respuesta agotado "+parametrized_url, ex);
		}
		catch(IOException ex){
			logger.error("Error en la consulta "+parametrized_url, ex);
		}
		return "";
	}

	/**
	 * Realiza una petici�n POST a un servicio Web
	 * @param service_url URL del servicio
	 * @param request petici�n
	 * @return respuesta del servicio
	 */
	protected static String doSearch(String service_url, String request){
		try{
			BusquedaSimple.logger.info("Peticion a "+service_url);
			URL url = new URL(service_url);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);

			logger.info(request);

			connection.setRequestProperty("Content-Type", "text/xml"); 
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(request.getBytes().length));
			connection.connect();
			OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			wr.write(request);
			wr.flush();

			//read the result from the server
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				sb.append(line + '\n');
			}

			//System.out.println(sb.toString());
			connection.disconnect();
			BusquedaSimple.logger.info("Respuesta de "+service_url);
			return sb.toString();
		}
		catch(MalformedURLException ex){
			logger.error("Error en la consulta "+request+ " al servicio "+service_url, ex);
			ex.printStackTrace();
		}
		catch(IOException ex){
			logger.error("Error en la consulta "+request+ " al servicio "+service_url, ex);
		}
		return "";
	}
	/*
	public static void generaToponimos(){
		int num=3000;
		int inicio=7005;
		String peticion = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><wfs:GetFeature version=\"1.1.0\" outputFormat=\"text/xml; subtype=gml/3.1.1\"    xmlns:wfs=\"http://www.opengis.net/wfs\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:ogc=\"http://www.opengis.net/ogc\" sortby=\"app:municipio\" startPosition=\""+inicio+"\" maxFeatures=\""+num+"\" >    <wfs:Query propertyName=\"app:identidad\" typeName=\"app:Nombres\" > "+
		"       <ogc:Filter><ogc:And><ogc:Not><ogc:PropertyIsEqualTo>    <ogc:PropertyName>xutm</ogc:PropertyName>    <ogc:Literal>0</ogc:Literal></ogc:PropertyIsEqualTo></ogc:Not><ogc:Not><ogc:PropertyIsEqualTo>    <ogc:PropertyName>yutm</ogc:PropertyName>    <ogc:Literal>0</ogc:Literal></ogc:PropertyIsEqualTo></ogc:Not><ogc:Not><ogc:PropertyIsEqualTo>    <ogc:PropertyName>cmunine</ogc:PropertyName>    <ogc:Literal>0</ogc:Literal></ogc:PropertyIsEqualTo> </ogc:Not> </ogc:And>       </ogc:Filter>    </wfs:Query></wfs:GetFeature>";
		String resultado = doSearch("http://idearagon.aragon.es/toponimiaWFS",peticion);
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new ByteArrayInputStream(resultado.getBytes()));
			doc.getDocumentElement().normalize();

			// Parsear la respuesta para obtener el n�mero de resultados

			NodeList entidades = doc.getElementsByTagName("app:Nombres");

			for (int i=0; i<entidades.getLength(); i++){
				NodeList atributos = entidades.item(i).getChildNodes();
				String nombre=null;
				String muni=null;
				for (int j=0; j<atributos.getLength();j++){
					if (atributos.item(j).getNodeName().equalsIgnoreCase("app:nombre")){
						nombre = atributos.item(j).getFirstChild().getNodeValue();
					}
					else if (atributos.item(j).getNodeName().equalsIgnoreCase("app:cmunine")){
						muni = atributos.item(j).getFirstChild().getNodeValue();
					}
					if ((nombre!=null)&&(muni!=null)){
						break;
					}
				}
				System.out.println(nombre+"#"+muni);
			}



		}
		catch (Exception ex){
			ex.printStackTrace();
		}
	}*/

	public static void main(String[] args) {
		//initProperties("E:\\IDEARIUM\\workspace\\SimpleSearchService\\src\\main\\webapp\\WEB-INF\\config\\busquedaSimple.properties");
		System.out.println(args[0]+" -> "+buscar(args[0],"1","10",null,/*"612850,4538825 781150,4658175"*/null,"E:\\IDEARIUM\\workspace\\SimpleSearchService\\src\\main\\webapp\\WEB-INF\\config\\busquedaSimple.properties"));

		System.out.println("Fin");
	}
}
