package es.ideariumConsultores.search.simple;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

public class Servlet extends HttpServlet {

	/**
	 * Inicializa el servlet. Carga el fichero de propiedades
	 * @param servletConfig
	 */
	public void init(ServletConfig servletConfig) throws ServletException
	{
		super.init(servletConfig);
		
		BusquedaSimple.initProperties(/*servletConfig.getInitParameter("properties_path")*/
				getServletContext().getRealPath("/WEB-INF/config")+"/busquedaSimple.properties");
	}

	/**
	 * petici�n por GET
	 * @param request petici�n que deber� contener el par�metro texto con la cadena a buscar y opcionalmente
	 * los par�mtros inicio y total para indicar la el primer resultado y el total a devolver y el par�metro bbox
	 * para restringir la b�squeda a dicha regi�n geogr�fica.
	 * @param response resultados de la b�squeda
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		BusquedaSimple.conexError.clear();
		BusquedaSimple.logger.info("****Inicio petición******");
		try{
			request.setCharacterEncoding("UTF-8");
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		String op = request.getParameter("REQUEST");
		if ((op!=null)&&(op.equalsIgnoreCase("LOG"))){
			try{
				Properties logProp = new Properties();

				logProp.load(new FileInputStream(getServletContext().getRealPath("/WEB-INF/classes")+"/log4j.properties"));
				FileInputStream log = new FileInputStream(logProp.getProperty("log4j.appender.A1.File"));

				ServletOutputStream sos = response.getOutputStream();
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/file");
				response.setHeader("Content-Disposition", "attachment; filename=\"SimpleSearchService.log\"");
				byte[] logBytes = new byte[512];
				while (log.read(logBytes)>0){
					sos.write(logBytes);
					logBytes = new byte[512];
				}
				sos.flush();
			}
			catch(Exception ex){
				try{
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
				}catch(IOException ex2){
					BusquedaSimple.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
					return;
				}
				BusquedaSimple.logger.error("Error en la exportación del log",ex);
			}
		}
			else{
		BusquedaSimple.logger.debug("Encoding "+request.getCharacterEncoding());
		String texto=request.getParameter("texto").trim();//Quitamos espacios en blanco
		BusquedaSimple.logger.debug("Texto "+texto);
		String indice=request.getParameter("inicio");
		if ((indice == null) || (indice.length() ==0)){
			indice ="1";
		}
		
		String maxResults=request.getParameter("maxResults");
		if ((maxResults != null) && (maxResults.length() ==0)){
			maxResults =null;
		}
		String total=request.getParameter("total");
		if ((total == null) || (total.length() ==0)){
			total ="20";
		}
		if ((maxResults !=null)&&(Integer.parseInt(maxResults)<Integer.parseInt(total))){
			total = maxResults;
		}
		String bbox=request.getParameter("bbox");
		if ((bbox != null) && (bbox.length() ==0)){
			bbox =null;
		}
		String app = request.getParameter("app");
		if ((app != null) && (app.length() ==0)){
			app = "";
		}
		BusquedaSimple.logger.info("texto "+texto + "(app: " + app + ")");

		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage outgoingMessage = messageFactory.createMessage();

			SOAPPart soappart = outgoingMessage.getSOAPPart();
			SOAPEnvelope envelope = soappart.getEnvelope();
			SOAPBody body = envelope.getBody();
			SOAPBodyElement responseElement = body.addBodyElement(envelope.createName("SearchResponse"));
			
			// realizar la b�squeda
			ArrayList<Resultado> resultados = BusquedaSimple.buscar(texto,indice.trim(),total.trim(),maxResults,bbox, false, app);

			// componer la respuesta con los resultados de la b�squeda
			for (Resultado resultado: resultados){
				SOAPElement searchResult = responseElement.addChildElement(envelope.createName("SearchResult"));
				searchResult.addChildElement(envelope.createName("Type")).addTextNode(resultado.type);
				searchResult.addChildElement(envelope.createName("Count")).addTextNode(new Integer(resultado.count).toString());
				if(resultado.list != null)
					searchResult.addChildElement(envelope.createName("List")).addTextNode(resultado.list);
			}
			BusquedaSimple.logger.info("****Fin petición******");
			// enviar la respuesta
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("text/xml; charset=\"utf-8\"");
			OutputStream os = response.getOutputStream();
			outgoingMessage.writeTo(os);
			os.flush();
			BusquedaSimple.logger.info("****Fin envío******");
		} 
		catch(Throwable e) {
			BusquedaSimple.logger.error("Error en la búsqueda de "+texto, e);
			try{
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
			catch(Exception ex){
				
			}
		}
			}
	}
}
