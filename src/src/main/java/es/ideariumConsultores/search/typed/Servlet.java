package es.ideariumConsultores.search.typed;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import es.ideariumConsultores.search.simple.BusquedaSimple;
import es.ideariumConsultores.search.simple.Resultado;

public class Servlet extends HttpServlet {
	//	HashMap<String, String> ft_fields;// Atributos de ArcXML por los que buscar 
	//HashMap<String, String> ft_fields_nofon; // Atributos de ArcXML por los que buscar sin fon�tica (solo features con atributo relleno con fon�tica)

	public void init(ServletConfig servletConfig) throws ServletException
	{
		super.init(servletConfig);
		Properties properties = new Properties();
		try{
			properties.load(new FileInputStream(getServletContext().getRealPath("/WEB-INF/config")+"/busquedaSimple.properties"));
		}
		catch(Exception ex){
			//logger.error("Error al cargar el fichero de propiedades "+propertiesPath, ex);
			//	ex.printStackTrace();	
		}
		// cargar los campos de b�squeda de cada feature ArcIMS
		/*		ft_fields=new HashMap<String, String>();
		ft_fields_nofon=new HashMap<String, String>();
		Enumeration props = properties.propertyNames();
		while (props.hasMoreElements()){
			Object prop = props.nextElement();
			System.out.println(properties.getProperty(prop.toString())+" -> "+ properties.getProperty(prop.toString().replaceFirst("_FTTYPE", "_FIELD")));

			if (prop.toString().endsWith("_FTTYPE")){
				ft_fields.put(properties.getProperty(prop.toString()), properties.getProperty(prop.toString().replaceFirst("_FTTYPE", "_FIELD")));
				if (properties.getProperty(prop.toString().replaceFirst("_FTTYPE", "_FIELD_NOFON"))!=null){
					ft_fields_nofon.put(properties.getProperty(prop.toString()), properties.getProperty(prop.toString().replaceFirst("_FTTYPE", "_FIELD_NOFON")));
				}
			}

		}*/

	}



	/**
	 * petici�n por GET
	 * @param request petici�n que deber� contener el par�metro texto con la cadena a buscar y opcionalmente
	 * los par�mtros inicio y total para indicar la el primer resultado y el total a devolver y el par�metro bbox
	 * para restringir la b�squeda a dicha regi�n geogr�fica.
	 * @param response resultados de la b�squeda
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException
	{
		BusquedaSimple.conexError.clear();
		String texto=request.getParameter("texto").trim();//Quitamos espacios en blanco
		try{
			request.setCharacterEncoding("UTF-8");
		}
		catch(Exception ex){
			ex.printStackTrace();
		}


		String bbox=null;
		String maxResults=request.getParameter("maxResults"); // m�ximo num de resultados a buscar
		if ((maxResults != null) && (maxResults.length() ==0)){
			maxResults =null;
		}

		String app = request.getParameter("app");
		if ((app != null) && (app.length() ==0)){
			app = "";
		}
		String type = request.getParameter("type");

		String muni = request.getParameter("muni");

		BusquedaSimple.logger.info("texto "+texto +" muni "+muni +"tipo "+type + "(app: " + app + ")");

		try {
			if (type == null){
				response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar el parámetro type");
				return;
			}
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage outgoingMessage = messageFactory.createMessage();

			SOAPPart soappart = outgoingMessage.getSOAPPart();
			SOAPEnvelope envelope = soappart.getEnvelope();
			SOAPBody body = envelope.getBody();
			SOAPBodyElement responseElement = body.addBodyElement(envelope.createName("SearchResponse"));

			// realizar la b�squeda
			int inicio = 1;
			int max =Integer.MAX_VALUE; 
			String conexion = "conexSSS_carto";
			if(maxResults != null){
				max=Integer.parseInt(maxResults);
			}
			String portal="";
			ArrayList<Resultado> resultados = new ArrayList<Resultado>();
			if (type.equalsIgnoreCase("toponimo")){
				resultados = searchToponimos(texto ,  muni,inicio,  max,  max,  bbox);
			}
			else if (type.equalsIgnoreCase("coordenadas")){
				resultados = BusquedaSimple.buscar(texto, "1",new Integer(max).toString(),new Integer(max).toString(), null,false, app);
			}
			else{

				boolean conMuni=false;
				if ((type.equalsIgnoreCase("BusParUrb")||type.equalsIgnoreCase("ParUrb")||type.equalsIgnoreCase("BusSIGPAC"))){
					if((texto.indexOf(":")>0)||(texto.indexOf("-")>0)){ 

						texto = BusquedaSimple.formatRefCat(texto);
					}
					if (type.equalsIgnoreCase("BusParUrb")){
						conexion = "conexSSS_catrus";
					}
					else if (type.equalsIgnoreCase("ParUrb")){
						conexion = "conexSSS_caturb";
					}
					
					else{
						conexion = "conexSSS_sigpac";
					}
				}
				else if ((type.indexOf("v206_")==0)|| (type.indexOf("zonas_salud")>=0)|| (type.indexOf("sector_sanitario")>=0)){
					conexion = "conexSSS_slud";
				}
				else if (type.equalsIgnoreCase("instalaciones_sanitarias")){
					conexion = "conexSSS_idearagon";
				}
				else if (type.equalsIgnoreCase("TroidesV")	&&(texto.matches(".+ [0-9]+"))){ 
					// quitar el número de portal
					portal =  texto.substring(texto.lastIndexOf(" ")).trim();
					texto = texto.substring(0, texto.lastIndexOf(" ")).trim();

					conMuni=((muni != null)&&(muni.length()>0));


				}
				else if (type.equalsIgnoreCase("BusCallUrb")){
					conMuni=((muni != null)&&(muni.length()>0));
				}
				/*if (type.equalsIgnoreCase("BusParUrb")){
					if ((texto.matches("[0-9]+"))&&(texto.length()==18)){
						field="REFCAT";
					}
					else {
						field="REFPAR";
					}
				}*/
				// b�squeda exacta sin fon�tica
				Resultado resultado=null;
				String field = BusquedaSimple.ATTR_NOMBRE;
				if (BusquedaSimple.esEntero(texto)){
					field = BusquedaSimple.ATTR_CODIGO;
				}
				else if ((type.equalsIgnoreCase("BusParUrb")||type.equalsIgnoreCase("ParUrb")||type.equalsIgnoreCase("BusSIGPAC"))){
					// devolver resultado vacío
					BusquedaSimple.logger.info("No busco parcelas por texto");
					resultado =  new Resultado(type,0,null);
				}
				if (resultado==null){
				if (conMuni){
					resultado = BusquedaSimple.doDBSearch(new String[]{texto, BusquedaSimple.getFonetica(muni)},type,new String[]{field, BusquedaSimple.ATTR_MUNICIPIO},null
					,inicio,max,max,conexion,false);
				}
				else{
					resultado= BusquedaSimple.doDBSearch(texto, type,
							field,inicio,max,max,conexion,false);
				}


				BusquedaSimple.logger.info("Resultado: "+resultado.count);
				if (resultado.count <=0){ // no hya resultados exactos sin fon�tica
					// se puede buscar con fon�tica

					String txt_buscar = BusquedaSimple.getFonetica(texto);
					// b�sqeuda exacta con fon�tica
					if (conMuni){
						resultado = BusquedaSimple.doDBSearch(new String[]{txt_buscar, BusquedaSimple.getFonetica(muni)},type,new String[]{ BusquedaSimple.ATTR_FONETICA, BusquedaSimple.ATTR_MUNICIPIO},null
						,inicio,max,max,conexion,false);
					}
					else{
						resultado = BusquedaSimple.doDBSearch(txt_buscar, type,
								BusquedaSimple.ATTR_FONETICA,inicio,max,max,conexion,false);
					}

					if (resultado.count <=0){
						// b�squeda parecidos con fon�tica
						if (conMuni){
							resultado = BusquedaSimple.doDBSearch(new String[]{txt_buscar, BusquedaSimple.getFonetica(muni)},type,new String[]{ BusquedaSimple.ATTR_FONETICA, BusquedaSimple.ATTR_MUNICIPIO},null
							,inicio,max,max,conexion,true);
						}
						else{
							resultado= BusquedaSimple.doDBSearch(txt_buscar, type,
									BusquedaSimple.ATTR_FONETICA,inicio,max,max,conexion,true);
						}

					}
					/*}
					else if (!(texto.matches("[0-9]+"))){  // no se puede buscar con fon�tica y el texto no es c�digo
						// b�squeda parecidos sin fon�tica
						resultado= BusquedaSimple.doDBSearch(texto, type,
							(field_nofon != null ? field_nofon:field),inicio,max,max,environment,true);
					}*/

				}
				}
BusquedaSimple.logger.info("la conex:"+conexion);

				resultados.add(resultado);
			}


			// componer la respuesta con los resultados de la b�squeda
			for (Resultado resultado: resultados){
				SOAPElement searchResult = responseElement.addChildElement(envelope.createName("SearchResult"));
				String tipo = type;
				if (tipo.equalsIgnoreCase("toponimo")&&app.equalsIgnoreCase("visor3D")){
					tipo="nombreAlguna";
				}
				else if (type.equalsIgnoreCase("TroidesV")){
					tipo = type+"_"+portal;
				}
				searchResult.addChildElement(envelope.createName("Type")).addTextNode(tipo);
				searchResult.addChildElement(envelope.createName("Count")).addTextNode(new Integer(resultado.count).toString());
				if(resultado.list != null)
					searchResult.addChildElement(envelope.createName("List")).addTextNode(resultado.list);
			}

			// enviar la respuesta
			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("text/xml; charset=\"utf-8\"");
			OutputStream os = response.getOutputStream();
			outgoingMessage.writeTo(os);
			os.flush();
		} 
		catch(Throwable e) {
			BusquedaSimple.logger.error("Error en la búsqueda de "+texto, e);
			try{
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
			catch(Exception ex){

			}
		}
	}

	public ArrayList<Resultado> searchToponimos(String texto ,String muni, int inicio, int totalInt, int max, String bbox){
		/*String texto_wfsg=texto;
		try{
			texto_wfsg=upperSpecial2Hex(java.net.URLDecoder.decode(texto, "UTF-8"));
		}
		catch(Exception ex){
			BusquedaSimple.logger.error("Error al normalizar el texto "+texto, ex);
		}
		 */

		Resultado toponimos;
		boolean hayMuni=false;
		boolean esMuniINE=false;
		if ((muni != null)&&(muni.length()>0)){
			hayMuni=true;
			esMuniINE=(BusquedaSimple.esEntero(muni)&&(muni.length()==5));

			if (!esMuniINE){
				muni=BusquedaSimple.getFonetica(muni);
			}
			toponimos=BusquedaSimple.doDBSearch(new String[]{texto,muni},"toponimo",new String[]{BusquedaSimple.ATTR_NOMBRE,(esMuniINE ? BusquedaSimple.ATTR_MUNI_INE:BusquedaSimple.ATTR_MUNICIPIO)},null,inicio,totalInt,max, BusquedaSimple.properties.getProperty("TOPO_DB","conexSSS_carto"),false);

		}
		else{
			toponimos=BusquedaSimple.doDBSearch(texto,"toponimo",BusquedaSimple.ATTR_NOMBRE,inicio,totalInt,max, BusquedaSimple.properties.getProperty("TOPO_DB","conexSSS_carto"),false);

		}
		ArrayList<Resultado> resultados = new ArrayList<Resultado>();

		int count = 0;

		count += toponimos.count;
		if (count>0){
			resultados.add(toponimos);
		}

		if (count <=0){ // buscar con fon�tica solo si no hay resultados sin fon�tica
			String texto_fon = BusquedaSimple.getFonetica(texto);
			Resultado toponimos_fon;
			if (hayMuni){
				toponimos_fon = BusquedaSimple.doDBSearch(new String[]{texto_fon,muni},"toponimo",new String[]{BusquedaSimple.ATTR_FONETICA,(esMuniINE ? BusquedaSimple.ATTR_MUNI_INE:BusquedaSimple.ATTR_MUNICIPIO)},null,inicio,totalInt,max, BusquedaSimple.properties.getProperty("TOPO_DB","conexSSS_carto"),false);
			}
			else{
				toponimos_fon=BusquedaSimple.doDBSearch(texto_fon,"toponimo",BusquedaSimple.ATTR_FONETICA,inicio,totalInt,max, BusquedaSimple.properties.getProperty("TOPO_DB","conexSSS_carto"),false);

			}

			count += toponimos_fon.count;
			if (count>0){
				resultados.add(toponimos_fon);

			}
			if (count <=0){ // buscar parecidos con fon�tica solo si no hay resultados exactos con fon�tica
				Resultado toponimos_fon_like;

				if (hayMuni){
					toponimos_fon_like = BusquedaSimple.doDBSearch(new String[]{texto_fon,muni},"toponimo",new String[]{BusquedaSimple.ATTR_FONETICA,(esMuniINE ? BusquedaSimple.ATTR_MUNI_INE:BusquedaSimple.ATTR_MUNICIPIO)},null,inicio,totalInt,max, BusquedaSimple.properties.getProperty("TOPO_DB","conexSSS_carto"),true);
				}
				else{

					toponimos_fon_like=BusquedaSimple.doDBSearch(texto_fon,"toponimo",BusquedaSimple.ATTR_FONETICA,inicio,totalInt,max, BusquedaSimple.properties.getProperty("TOPO_DB","conexSSS_carto"),true);

				}				
				resultados.add(toponimos_fon_like);


			}
		}

		return resultados;
	}


}
