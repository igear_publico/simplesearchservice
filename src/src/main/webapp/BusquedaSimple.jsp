<%@page language='java' contentType='text/html;charset=UTF-8'%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.lang.*"%>
<%@page import = "es.ideariumConsultores.search.simple.*" %>
<html>
<body>
<jsp:useBean id="buscador" class="es.ideariumConsultores.search.simple.BusquedaSimple" scope="request"/>
<%
// Jsp que busca entidades geogr�ficas a partir de una cadena de texto (b�squeda caja unica)
// @documento: pasarToponimos.jsp
// @author: Raquel
// @fecha: Octubre 2012

    String texto=request.getParameter("texto").trim();//Quitamos espacios en blanco
	String indice=request.getParameter("inicio");
	if ((indice == null) || (indice.length() ==0)){
		indice ="1";
	}
	String total=request.getParameter("total");
	if ((total == null) || (total.length() ==0)){
		total ="20";
	}
	String bbox=request.getParameter("bbox");
	if ((bbox != null) && (bbox.length() ==0)){
		bbox =null;
	}
   	out.println(buscador.buscar(texto,indice.trim(),total.trim(),bbox,request.getRealPath("/")+"\\WEB-INF\\config\\busquedaSimple.properties"));

%>
</body>
</html>